# Lightweight menu framework(C)

#### Description
1.  The multi-level menu is realized by linked list

2.  The menu frame is more independent and does not couple the key module and the display module, that is, the menu display effect is more free

3.  Easy to use

#### Software Architecture
Software architecture description

#### Instructions

1.  Initialize function Menu_Init before use to set the contents of the main menu

2.  Cycle call function Menu_Task, which is used to handle menu display and execute related callback functions
   
3.  Use other functions to control the menu interface

#### Author
1.  CSDN  blog.csdn.com
2.  email const_zpc@163.com
3.  Welcome to comment and maintain

